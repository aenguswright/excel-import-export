export interface Todo {
  id: number;
  content: string;
}

export interface Meta {
  totalCount: number;
}

export class Nation { id: number = 0; ioc: string = '' }
export class Show {
  id: number
  name: string
  nation: Nation | undefined
  constructor () {
    this.id = 0
    this.name = ''
    this.nation = undefined
  }
  public parseObject(id : number, name : string, nation : Nation) {
    this.id = id
    this.name = name
    this.nation = nation
  }
}
export type Competitors = Array<Competitor>
export type Competitor = Record<string, unknown>
export type Selectable = { value : string | number, label: string }
export type Header = { source: string, databaseKey: string }
export type ZonedInstant = { instant: unknown }
export type AthleteComment = { message: string, show: number, zonedInstant: ZonedInstant }
export class Federation {
  id: number = 0
  nation: Nation = new Nation()
}
export class NationalRegistration {
  number: string = ''
  federation: Federation = new Federation()
}
export class Athlete {
  firstName: string = ''
  familyName: string = ''
  feiId: string | undefined = ''
  nationalRegistrations: Array<NationalRegistration> = [new NationalRegistration()]
  dateOfBirth: Date | undefined = new Date()
  gender: string | undefined = undefined
  nation: Nation = new Nation()
  email: string | undefined = undefined
  comment: Array<AthleteComment> = []
}
export class Horse {
  name: string = ''
  feiId: string | undefined = undefined
  nationalRegistrations: Array<NationalRegistration> = [new NationalRegistration()]
  dateOfBirth: Date | undefined = undefined
  dam: string | undefined = undefined
  sire: string | undefined = undefined
  sireOfDam: string | undefined = undefined
  gender: string | undefined = undefined
  studbook: string | undefined = undefined
  breeder: string | undefined = undefined
  owner: string | undefined = undefined
  permanentBridleNumber: string | undefined = undefined
}
export class Entry {
  id : number | undefined = undefined
  athlete : Athlete = new Athlete()
  horse : Horse = new Horse()
  cno : number | undefined = undefined
  zonedInstant : ZonedInstant | undefined = undefined
  division: string | undefined = undefined
  competitionNumber: string = ''
  status : string = 'UPCOMING'
}
