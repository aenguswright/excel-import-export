const { GraphQLClient } = require('graphql-request')
const { Competitions } = require('../queries/competitions')
const { Competitors } = require('../queries/competitors')
const { Athletes } = require('../queries/athletes')
const { Horses } = require('../queries/horses')
const { Nations } = require('../queries/nations')
const { Shows } = require('../queries/shows')

export class Api {
  prototype : unknown
  graphQl : any
  competitionsApi : any
  competitorsApi : any
  athletesApi : any
  horsesApi : any
  nationsApi : any
  showsApi : any
  constructor (serverAddress : string, token : string) {
      this.graphQl = new GraphQLClient(`${serverAddress}`, {
      headers: {
        Authorization: token
      }
    })
    this.competitionsApi = new Competitions(this.graphQl)
    this.competitorsApi = new Competitors(this.graphQl)
    this.athletesApi = new Athletes(this.graphQl)
    this.horsesApi = new Horses(this.graphQl)
    this.nationsApi = new Nations(this.graphQl)
    this.showsApi = new Shows(this.graphQl)
  }
}
