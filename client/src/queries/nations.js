class Nations {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.nationParams = `{
        id
        ioc
        name
    }`
  }

  findNations (term) {
    if (!term) {
      return Promise.resolve(null)
    }
    let graphQl = this.graphQl
    let query = `query FindNations($term: String!) {
        findNations(term: $term) ` + this.nationParams + `
    }`
    let variables = {
      term: term
    }
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        let nations = response.findNations
        if (nations) {
          resolve(nations)
        } else {
          resolve(null)
        }
      })
    })
  }
}

module.exports = {
  Nations: Nations
}
