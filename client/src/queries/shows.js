class Shows {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.nationParams = `{
          id
          ioc
          name
      }`
  }

  getCompetitions (showId) {
    let graphQl = this.graphQl
    let query = `{
        competitions(showId: ${showId}) {
          id
          name
          number
          status
          discipline
          zonedInstant {
            instant
          }
          arena {
            id
            name
          }
        }
        show(id: ${showId}) {
          id
          name
          nation {
            id
            ioc
          }
        }
      }`
    let variables = {
      showId: showId
    }
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        if (response) {
          resolve(response)
        } else {
          reject(new Error('response was undefined'))
        }
      })
    })
  }
}

module.exports = {
  Shows: Shows
}
