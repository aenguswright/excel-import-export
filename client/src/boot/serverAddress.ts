import { boot } from 'quasar/wrappers'

export default boot(({ Vue }) => {
    Vue.prototype.$serverAddress = JSON.stringify('wss://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/')
})