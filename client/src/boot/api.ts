import api from '../queries/api'
import { boot } from 'quasar/wrappers'
// leave the export, even if you don't use it
export default boot(({ Vue }) => {
  Vue.prototype.$api = api
})
