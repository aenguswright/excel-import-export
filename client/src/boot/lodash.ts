import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import { boot } from 'quasar/wrappers'

// leave the export, even if you don't use it
export default boot(({ Vue }) => {
  Vue.use(VueLodash, { lodash: lodash })
})
