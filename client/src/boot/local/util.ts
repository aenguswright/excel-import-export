// import lodash from 'lodash'
import dayjs from 'dayjs'
import { Notify } from 'quasar'
import { Competitor, Competitors, Header, Selectable, Entry, ZonedInstant, AthleteComment } from '../../components/models'

type Utility = { assignEntity : Function, errorToast : Function, findKeyName : Function }
const utility : Utility = {

  errorToast: (message : string) => {
    Notify.create({
      message: message,
      color: 'negative'
    })
  },

  assignEntity: function (key : string, value : string | number, base : Entry) {
    if (!value) return false
    const stringValue:string = (typeof value === 'string') ? value : ''
    const numberValue:number | undefined = (typeof value === 'number') ? value : undefined
    let showId : number
    switch (key) {
      case 'athlete.firstName': base.athlete.firstName = stringValue; break
      case 'athlete.familyName': base.athlete.familyName = stringValue; break
      case 'athlete.fullName':
        let values : Array<string> = String(stringValue).trim().split(' ')
        base.athlete.firstName = values.shift() || '';
        base.athlete.familyName = values.join(' ').trim(); break
      case 'athlete.feiId': base.athlete.feiId = stringValue; break
      case 'athlete.nationalRegistrations.number':
        // TODO getFederationByNationOfShow
        base.athlete.nationalRegistrations[0].number = stringValue; break
      case 'athlete.dateOfBirth': base.athlete.dateOfBirth = dayjs(stringValue); break
      case 'athlete.gender': base.athlete.gender = stringValue; break
      case 'athlete.nation.ioc': base.athlete.nation.ioc = stringValue; break
      case 'athlete.email': base.athlete.email = stringValue; break
      case 'athlete.comment.message': base.athlete.comment.push(<AthleteComment>{
        message: stringValue,
        show: showId,
        zonedInstant: {
          instant: dayjs()
        }
      }); break
      case 'horse.name': base.horse.name = stringValue; break
      case 'horse.feiId': base.horse.feiId = stringValue; break
      case 'horse.nationalRegistrations.number': 
      // TODO getFederationByNationOfShow
      base.horse.nationalRegistrations[0].number = stringValue; break
      case 'horse.dateOfBirth': base.horse.dateOfBirth = dayjs(stringValue); break
      case 'horse.sire': base.horse.sire = stringValue; break
      case 'horse.dam': base.horse.dam = stringValue; break
      case 'horse.sireOfDam': base.horse.sireOfDam = stringValue; break
      case 'horse.gender': base.horse.gender = stringValue; break
      case 'horse.studbook': base.horse.studbook = stringValue; break
      case 'horse.breeder': base.horse.breeder = stringValue; break
      case 'horse.owner': base.horse.owner = stringValue; break
      case 'horse.permanentBridleNumber':
        base.horse.permanentBridleNumber = stringValue;
        base.cno = numberValue; break
      case 'competitor.startTime': base.zonedInstant = {
        instant: stringValue
      }; break
      case 'competitor.division': base.division = stringValue; break
      case 'competition.number': base.competitionNumber = stringValue; break
    }
    return base
  },

  findKeyName: function (header : Header) {
    const comparisonHeader = header
    const getHeader = (key : string) => {
      let regex = new RegExp(/(athlete|rider|full)+'?.?(full)?.?(name)/, 'gi')
      if (regex.test(key)) { return 'athlete.fullName' }

      regex = new RegExp(/(athlete|rider|first)+'?.?.?(first)?.?(name)/, 'gi')
      if (regex.test(key)) { return 'athlete.firstName' }

      regex = new RegExp(/(athlete|rider|family|last)+?'?.?.?(family|last)?.?(name)/, 'gi')
      if (regex.test(key)) { return 'athlete.familyName' }

      regex = new RegExp(/(athlete|rider)+?'?.?.?(fei)+?.?(id|number|no|#)/, 'gi')
      if (regex.test(key)) { return 'athlete.feiId' }
      
      regex = new RegExp(/((athlete|rider)+?'?.?.?(national|ea|usef|member)?.?(id|number|no|#|member|number))|((member)+.*(id|number|no|#))/, 'gi')
      if (regex.test(key)) { return 'athlete.nationalRegistrations.number' }
      
      regex = new RegExp(/(athlete|rider)+?'?.*(birth)/, 'gi')
      if (regex.test(key)) { return 'athlete.dateOfBirth' }
      
      regex = new RegExp(/(athlete|rider)+?'?.*(gender|sex)/, 'gi')
      if (regex.test(key)) { return 'athlete.dateOfBirth' }
      
      regex = new RegExp(/(nation|NF|country)+/, 'gi')
      if (regex.test(key)) { return 'athlete.dateOfBirth' }
      
      regex = new RegExp(/(email)+/, 'gi')
      if (regex.test(key)) { return 'athlete.email' }
      
      regex = new RegExp(/(note|message|comment)+/, 'gi')
      if (regex.test(key)) { return 'athlete.comment.message' }
      
      regex = new RegExp(/(horse)+'?s?.?(name)/, 'gi')
      if (regex.test(key)) { return 'horse.name' }
      
      regex = new RegExp(/(horse)+?'?.?.?(fei)+.?(id|number|no|#)/, 'gi')
      if (regex.test(key)) { return 'horse.feiId' }
      
      regex = new RegExp(/((horse)+'?.?.?(national|ea|usef|member)?.?(id|no|#|member|number))|((member)+.*(id|number|no|#)+)/, 'gi')
      if (regex.test(key)) { return 'horse.nationalRegistrations.number' }
      
      regex = new RegExp(/(horse)?'?.?.?(birth|foal)/, 'gi')
      if (regex.test(key)) { return 'horse.dateOfBirth' }
      
      regex = new RegExp(/(sire)+/, 'gi')
      if (regex.test(key)) { return 'horse.sire' }
      
      regex = new RegExp(/(dam)+/, 'gi')
      if (regex.test(key)) { return 'horse.dam' }
      
      regex = new RegExp(/(((sireOfDam|sod)(s.?o.?d))+)|(grandsire)/, 'gi')
      if (regex.test(key)) { return 'horse.sireOfDam' }
      
      regex = new RegExp(/(horse)+.?(gender|sex)/, 'gi')
      if (regex.test(key)) { return 'horse.sireOfDam' }
      
      regex = new RegExp(/(horse).?(studbook|breed)/, 'gi')
      if (regex.test(key)) { return 'horse.studbook' }
      
      regex = new RegExp(/(horse).?(breeder)/, 'gi')
      if (regex.test(key)) { return 'horse.breeder' }
      
      regex = new RegExp(/(horse).?(owner)+s?/, 'gi')
      if (regex.test(key)) { return 'horse.owner' }
      
      regex = new RegExp(/((horse|competitor).?(number|bridle|#))|(cno|bridle)/, 'gi')
      if (regex.test(key)) { return 'horse.permanentBridleNumber' }
      
      regex = new RegExp(/(start)(time)/, 'gi')
      if (regex.test(key)) { return 'competitor.startTime' }
      
      regex = new RegExp(/(div|pony)/, 'gi')
      if (regex.test(key)) { return 'competitor.division' }
      
      regex = new RegExp(/((competition)+.?(number|no|#|nr|id))|((comp)+\s(number|no|#|nr|id))/, 'gi')
      if (regex.test(key)) { return 'competition.number' }
      return ''
    }
    header.databaseKey = getHeader(header.source)

  }
}
export default utility