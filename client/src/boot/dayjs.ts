import Dayjs from 'vue-dayjs-plugin'
import { boot } from 'quasar/wrappers'

// leave the export, even if you don't use it
export default boot(({ Vue }) => {
  Vue.use(Dayjs)
})
