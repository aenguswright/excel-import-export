import { boot } from 'quasar/wrappers'
import Util from './local/util'

// leave the export, even if you don't use it
export default boot(({ Vue }) => {
  Vue.prototype.$util = Util
})
