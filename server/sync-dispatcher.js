'use strict'
const _ = require('underscore')
const axios = require('axios')

class SyncMarksDispatcher {
  constructor (competitionId, competitionNumber, classDatabaseConnection, token, serverAddress) {
    this.competitionId = competitionId
    this.competitionNumber = competitionNumber
    this.classDatabaseConnection = classDatabaseConnection
    this.token = token
    this.serverAddress = serverAddress
  }

  async dispatch (competitor, competition) {
    return this.classDatabaseConnection.query(`SELECT idnRideID FROM tblRide WHERE lngEntryID = ${competitor.cno} AND sngClassID = ${this.competitionNumber}`)
      .then(async (result) => {
        if (result.length === 0) return console.error('Could not match our competitor to remote')
        let idnRideID = result[0].idnRideID

        // First get projection
        if (competitor.individualTest) {
          try {
            competition.fullModeTest.exercises.sort((a, b) => a.number - b.number)
            let specialExercises = competition.fullModeTest.exercises.filter((a) => a.exerciseType === 'COLLECTIVE' || a.exerciseType === 'ARTISTIC').map((a) => a.number)
            let testMaxPoints = _.reduce(competition.fullModeTest.exercises, (memo, exercise) => memo + (exercise.coefficient * 10), 0)

            let url = this.serverAddress + 'api/export/projection/'
            const instance = axios.create({
              baseURL: url,
              headers: { 'Authorization': 'Bearer ' + this.token }
            })
            var response = await instance.get(competitor.id)
            await this.upsertMarksAndPenalties(response.data, specialExercises, idnRideID, competition.fullModeTest.penaltyRule, testMaxPoints)
            if (competitor.resultOfficialStatus === 'CONFIRMED' &&
              (competitor.status === 'UPCOMING' || competitor.status === 'FINISHED' || competitor.status === 'RUNNING')) {
              await this.updateRanksAndStatus(competitor, idnRideID)
            }
          } catch (err) {
            console.log(err)
          }
        } else if (competitor.testOfChoiceAssignments && competitor.testOfChoiceAssignments.length > 0) {
          try {
            for (var i = 0; i < competitor.testOfChoiceAssignments.length; i++) {
              var toc = competitor.testOfChoiceAssignments[i]
              let inputArr = []
              var filteredJudgeResults = competitor.judgeResults.filter((judgeResult) => judgeResult.competitionJudge.position === toc.position)
              filteredJudgeResults.forEach(judgeResult => {
                let markedExercises = []
                judgeResult.markedExercises.forEach((markedExercise) => {
                  markedExercises.push({
                    mark: markedExercise.mark,
                    number: markedExercise.number
                  })
                })
                inputArr.push({
                  firstErrorOfCourse: judgeResult.firstErrorOfCourse,
                  secondErrorOfCourse: judgeResult.secondErrorOfCourse,
                  technicalPenalty: judgeResult.technicalPenalty,
                  judgePosition: judgeResult.competitionJudge.position,
                  markedExercises: markedExercises
                })
              })

              toc.test.exercises.sort((a, b) => a.number - b.number)
              let specialExercises = toc.test.exercises.filter((a) => a.exerciseType === 'COLLECTIVE' || a.exerciseType === 'ARTISTIC').map((a) => a.number)
              let testMaxPoints = _.reduce(toc.test.exercises, (memo, exercise) => memo + (exercise.coefficient * 10), 0)

              await this.upsertMarksAndPenalties(inputArr, specialExercises, idnRideID, toc.test.penaltyRule, testMaxPoints)
            }

            if (competitor.resultOfficialStatus === 'CONFIRMED' &&
              (competitor.status === 'UPCOMING' || competitor.status === 'FINISHED' || competitor.status === 'RUNNING')) {
              await this.updateRanksAndStatus(competitor, idnRideID)
            }
          } catch (err) {
            console.log(err)
          }
        } else if (competition.fullModeTest) {
          try {
            competition.fullModeTest.exercises.sort((a, b) => a.number - b.number)
            let specialExercises = competition.fullModeTest.exercises.filter((a) => a.exerciseType === 'COLLECTIVE' || a.exerciseType === 'ARTISTIC').map((a) => a.number)
            let testMaxPoints = _.reduce(competition.fullModeTest.exercises, (memo, exercise) => memo + (exercise.coefficient * 10), 0)

            // Pre-Process to match projection data
            let inputArr = []
            competitor.judgeResults.forEach(judgeResult => {
              let markedExercises = []
              judgeResult.markedExercises.forEach((markedExercise) => {
                markedExercises.push({
                  mark: markedExercise.mark,
                  number: markedExercise.number
                })
              })
              inputArr.push({
                firstErrorOfCourse: judgeResult.firstErrorOfCourse,
                secondErrorOfCourse: judgeResult.secondErrorOfCourse,
                technicalPenalty: judgeResult.technicalPenalty,
                judgePosition: judgeResult.competitionJudge.position,
                markedExercises: markedExercises
              })
            })
            await this.upsertMarksAndPenalties(inputArr, specialExercises, idnRideID, competition.fullModeTest.penaltyRule, testMaxPoints)
            if (competitor.resultOfficialStatus === 'CONFIRMED' &&
              (competitor.status === 'UPCOMING' || competitor.status === 'FINISHED' || competitor.status === 'RUNNING')) {
              await this.updateRanksAndStatus(competitor, idnRideID)
            }
          } catch (err) {
            console.log(err)
          }
        }
      })
      .catch((error) => {
        throw new Error('Unable to query for the competitor because of: ' + error.message, error)
      })
  }

  async upsertMarksAndPenalties (judgeResults, specialExercises, rideId, penaltyRule, testMaxPoints) {
    for (var i = 0; i < judgeResults.length; i++) {
      let judgeResult = judgeResults[i]
      let position = judgeResult.judgePosition.toUpperCase()

      let tableName = `tblTestJudge${position}`

      let markEntries = {}
      judgeResult.markedExercises.forEach(exerciseMark => {
        // We have normal exercise
        let columnName = `sngS${exerciseMark.number}`
        // We have a special (artistic/collective) exercise
        if (specialExercises.indexOf(exerciseMark.number) !== -1) {
          let exerciseIndex = specialExercises.indexOf(exerciseMark.number) + 1
          columnName = `sngSC${exerciseIndex}`
        }
        if (exerciseMark.mark) {
          markEntries[columnName] = exerciseMark.mark
        }
        // Penalties
        let penalties = 0
        if (penaltyRule === 'FEI_STANDARD') {
          penalties = judgeResult.firstErrorOfCourse ? testMaxPoints * 0.02 : 0
        } else if (penaltyRule === 'FEI_YOUNG' || penaltyRule === 'FEI_PARA') {
          penalties = judgeResult.firstErrorOfCourse && judgeResult.secondErrorOfCourse ? testMaxPoints * 0.025 : judgeResult.firstErrorOfCourse ? 0.005 : 0
        } else if (penaltyRule === 'NATIONAL_POINTS_STANDARD') {
          penalties = judgeResult.firstErrorOfCourse && judgeResult.secondErrorOfCourse ? 6 : judgeResult.firstErrorOfCourse ? 2 : 0
        }

        if (judgeResult.technicalPenalty) {
          penalties += 2
        }
        if (penalties) {
          markEntries['sngErrors'] = penalties
        }
      })
      await this.classDatabaseConnection.query(`SELECT TOP 1 1 FROM ${tableName} WHERE idnRideID = ${rideId};`)
        .then(async (result) => {
          let upsertQuery = ''
          if (result.length === 0) {
            if (_.isEmpty(markEntries)) return // Don't do nothing
            upsertQuery = `INSERT INTO ${tableName} (idnRideID`
            for (let key in markEntries) {
              if (markEntries.hasOwnProperty(key)) {
                upsertQuery += `, ${key}`
              }
            }
            upsertQuery += `) VALUES (${rideId}`
            for (let key in markEntries) {
              if (markEntries.hasOwnProperty(key)) {
                upsertQuery += `, ${markEntries[key]}`
              }
            }
            upsertQuery += ');'
            console.log(upsertQuery)
          } else {
            if (_.isEmpty(markEntries)) return // Don't update nothing
            upsertQuery = `UPDATE ${tableName} SET `
            for (let key in markEntries) {
              if (markEntries.hasOwnProperty(key)) {
                upsertQuery += `${key} = ${markEntries[key]}, `
              }
            }
            upsertQuery += `WHERE idnRideID = ${rideId};`
            upsertQuery = upsertQuery.replace(', WHERE', ' WHERE')
            console.log(upsertQuery)
          }
          await this.classDatabaseConnection.execute(upsertQuery)
            .then((_) => {})
            .catch((error) => {
              throw new Error('There was an error upserting the marks to the database: ' + error)
            })
        })
        .catch((error) => {
          throw new Error('There was an error querying for the existance of a result' + error)
        })
    }
  }

  async updateRanksAndStatus (competitor, rideId) {
    let updateQuery = `UPDATE tblRide SET blnConfirmed = True`
    for (var i = 0; i < competitor.judgeResults.length; i++) {
      let judgeResult = competitor.judgeResults[i]
      updateQuery += `, bytRank${judgeResult.competitionJudge.position} = ${judgeResult.judgeRank}`
    }
    updateQuery += `, bytPlace = ${competitor.rankOfficial}`
    updateQuery += ` WHERE idnRideID = ${rideId};`

    await this.classDatabaseConnection.execute(updateQuery)
      .then((_) => {
        console.log('Finished and confirmed competitor ' + competitor.cno)
      })
      .catch((error) => {
        throw new Error('Unable to finish and confirm competitor because of: ' + error.message, error)
      })
  }
}

module.exports = SyncMarksDispatcher
