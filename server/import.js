const io = require('socket.io')()
const _ = require('lodash')
const axios = require('axios')
const moment = require('moment-timezone')
const SyncMarksDispatcher = require('./sync-dispatcher')
var ADODB = require('node-adodb')
ADODB.debug = true

const { Api } = require('./api')

var states = Object.freeze({
  'waitingForCredentials': 1,
  'operational': 2,
  'waitingForShowIds': 3,
  'retrievingBlackHorseData': 4,
  'retrievingLocalData': 5,
  'compareImportCompetitions': 6,
  'compareImportInternationalCompetitors': 7,
  'compareImportNationalCompetitors': 8,
  'exportNewMarks': 9,
  'done': 10
})

let api
let status = states.waitingForCredentials
let username
let password
let token
let nationalShowId
let internationalShowId
let timer
let nationalShow
let internationalShow
let nationalCompetitions
let internationalCompetitions
let fvNationalCompetitions
let fvInternationalCompetitions
let fvInternationalParaCompetitions
let fvNationalCompetitors
let fvInternationalCompetitors
let fvInternationalParaCompetitors
let competitionId
let comment = ''

// change to right path/data
let serverAddress = 'https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/' // https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/

let competitionTimeZone = 'America/New_York'


function log (msg) {
  console.log(msg)
  io.emit('log', {
    log: msg
  })
}

function getStatusDesc () {
  switch (status) {
    case states.waitingForCredentials: return 'waiting for credentials'
    case states.waitingForShowIds: return 'waiting for show IDs'
    case states.retrievingLocalData: return 'retrieving local data'
    case states.retrievingBlackHorseData: return 'retrieving black horse data'
    case states.compareImportCompetitions: return 'compare / import competitions'
    case states.compareImportInternationalCompetitors: return 'compare / import competitors'
    case states.compareImportNationalCompetitors: return 'compare / import competitors'
    case states.exportNewMarks: return 'export new marks'
    case states.done: return 'sync finished'
    case states.operational:
      if (!nationalShow) {
        return 'waiting for show IDs'
      } else {
        return 'operational'
      }
  }
}

function emitStatus () {
  io.emit('status', {
    classNationalDatabasePath: classNationalDatabasePath,
    classInternationalDatabasePath: classInternationalDatabasePath,
    classInternationalParaDatabasePath: classInternationalParaDatabasePath,
    nationalShowName: nationalShow ? nationalShow.name : null,
    internationalShowName: internationalShow ? internationalShow.name : null,
    status: getStatusDesc(),
    comment: comment,
    step: status
  })
}

function obtainToken () {
  axios.post(serverAddress + 'login', {
    email: username,
    password: password
  })
    .then(response => {
      token = response.headers.authorization
      api = new Api(serverAddress, token)
      log(`${username} logged in`)
      if (timer) {
        clearInterval(timer)
      }
      // also refresh the token after 50 minutes
      timer = setInterval(obtainToken, 1000 * 60 * 50)
      status = states.operational

      emitStatus()
    })
    .catch(error => {
      log(`failed to log in ${username} because of ${error}`)
      status = states.waitingForCredentials
      emitStatus()
    })
}

async function getLocalRider (riderId) {
  return coreDatabaseConnection.query(`SELECT * FROM [tblRider] WHERE idsRiderID = ${riderId};`)
    .then((riders) => {
      if (riders.length === 0) return Promise.reject(new Error(`Rider with id ${riderId} not found`))
      return Promise.resolve(riders[0])
    })
    .catch((error) => {
      console.error(`Failed to query rider: ${riderId} because of:`, error)
    })
}

async function getLocalHorse (horseId) {
  return coreDatabaseConnection.query(`SELECT * FROM [tblHorse] WHERE idsHorseID = ${horseId};`)
    .then((horses) => {
      if (horses.length === 0) return Promise.reject(new Error(`Horse with id ${horseId} not found`))
      return Promise.resolve(horses[0])
    })
    .catch((error) => {
      console.error(`Failed to query horse: ${horseId} because of:`, error)
    })
}

async function getLocalCompetitionSource (competition) {
  
  if (classNationalDatabaseConnection) {
    console.log('checking national competition')
    return classNationalDatabaseConnection.query(`SELECT * FROM [tblClass] WHERE chrText = "${competition.number}";`)
    .then((nationalCompetition) => {
      if (nationalCompetition.length > 0) return Promise.resolve({ competition: nationalCompetition[0], source: 'national' })
      return classInternationalDatabaseConnection.query(`SELECT * FROM [tblClass] WHERE chrText = "${competition.number}";`)
        .then((internationalCompetition) => {
          if (internationalCompetition.length > 0) return Promise.resolve({ competition: internationalCompetition[0], source: 'international' })
          return classInternationalParaDatabaseConnection.query(`SELECT * FROM [tblClass] WHERE chrText = "${competition.number}";`)
            .then((internationalParaCompetition) => {
              if (internationalParaCompetition.length > 0) return Promise.resolve({ competition: internationalParaCompetition[0], source: 'internationalPara' })
              else return Promise.reject(new Error(`Competition with id ${competition.id} not found`))
            })
        })
    })
    .catch((error) => {
      console.error(`Failed to query competition: ${competition.id} because of:`, error)
    })
  } else {
    return classInternationalDatabaseConnection.query(`SELECT * FROM [tblClass] WHERE chrText = "${competition.number}";`)
    .then((internationalCompetition) => {
      console.log('checking international competition: ' + internationalCompetition)
      if (internationalCompetition.length > 0) return Promise.resolve({ competition: internationalCompetition[0], source: 'international' })
      return classInternationalParaDatabaseConnection.query(`SELECT * FROM [tblClass] WHERE chrText = "${competition.number}";`)
        .then((internationalParaCompetition) => {
          console.log('checking international para competition: ' + internationalParaCompetition)
          if (internationalParaCompetition.length > 0) return Promise.resolve({ competition: internationalParaCompetition[0], source: 'internationalPara' })
          else return Promise.reject(new Error(`Competition with id ${competition.id} not found`))
        })
    })
  }
}

async function getLocalCompetitions (databaseSource) {
  return databaseSource.query(`SELECT * FROM [tblClass] c LEFT JOIN [tblDate] d ON d.idsDateID = c.idsDateID`)
    .then((competitions) => {
      if (competitions.length === 0) {
        return Promise.reject(new Error(`No competitions found`))
      }
      return Promise.resolve(competitions)
    })
    .catch((error) => {
      console.error(`Failed to query competitions because of:`, error)
    })
}

async function getLocalCompetitors (databaseSource) {
  return databaseSource.query(localCompetitorQuery)
    .then((competitors) => {
      if (competitors.length === 0) {
        return Promise.reject(new Error(`No competitors found`))
      }
      return Promise.resolve(competitors)
    })

    .catch((error) => {
      console.error(`Failed to query competitors because of:`, error)
    })
}

async function getBHCompetitions (showId) {
  return api.showsApi.getCompetitions(showId)
    .then((response) => {
      if (!response.show) {
        return Promise.reject(new Error(`No show found`))
      }
      return Promise.resolve(response)
    })
    .catch((error) => {
      console.error(`Failed to query competitions because of:`, error)
    })
}

async function saveOrUpdateCompetitons (localCompetitions, bhCompetitions, show, type) {
  for (var i = 0; i < localCompetitions.length; i++) {
    let competition = localCompetitions[i]
    const compNumber = competition.chrText
    const compName = competition.chrName

    // only import if dtmStartTime is not null (for some reason an empty field in the DB turns into '1970-01-01T00:00:00Z' in the code..)
    if (competition.dtmStartTime && competition.dtmStartTime.length > 0 && competition.dtmStartTime !== '1970-01-01T00:00:00Z') {
      // Convert competitionDate to UTC from the defined TimeZone. This assumes that the date already is
    // in the defined timeZone
      let split = competition.dtmStartTime.toString().substring(11, 20)
      let momentTime = moment(competition.dtmDate).format('YYYY-MM-DD') + ' ' + split

      const bhCompetitionFound = _.find(bhCompetitions, function (c) { return c.number === compNumber })
      if (!bhCompetitionFound || bhCompetitionFound.length === 0) {
        console.log(compName + ' gets created as ' + type + ' competition')
        const competitionToBeSaved = { name: compName, number: compNumber }
        competitionToBeSaved.show = show
        competitionToBeSaved.zonedInstant = { instant: momentTime, timeZone: competitionTimeZone }
        competitionToBeSaved.status = 'UPCOMING'
        competitionToBeSaved.paperless = true
        await api.competitionsApi.saveDressageCompetition(competitionToBeSaved)
      } else if (bhCompetitionFound && bhCompetitionFound.length > 0) {
      // check if update
        let isChanged = false
        let competitionToBeUpdated = bhCompetitionFound[0]
        if (competitionToBeUpdated.name !== compName) {
          isChanged = true
          competitionToBeUpdated.name = compName
        }
        if (competitionToBeUpdated.zonedInstant.instant !== momentTime) {
          isChanged = true
          competitionToBeUpdated.zonedInstant.instant = momentTime
        }
        if (isChanged) {
          await api.competitionsApi.saveDressageCompetition(competitionToBeUpdated)
        }
      }
    }
  }
}

async function importMissingAthlete (missingAthlete) {
  return api.athletesApi.fromMDB(missingAthlete)
    .then(async (athlete) => {
      return api.athletesApi.saveAthlete(athlete)
        .then((savedAthlete) => {
          comment = comment + '\n Athlete ' + savedAthlete.athlete.firstName + ' ' + savedAthlete.athlete.familyName + ' has been imported'
          return savedAthlete.athlete
        })
        .catch((_) => {
          console.error('There was a problem importing this Athlete ' + athlete.firstName + ' ' + athlete.familyName)
        })
    })
}

async function importMissingHorse (missingHorse) {
  let horse = api.horsesApi.fromMDB(missingHorse)
  return api.horsesApi.saveHorse(horse)
    .then((savedHorse) => {
      comment = comment + '\n Horse ' + savedHorse.horse.name + ' has been imported'
      return savedHorse.horse
    })
    .catch((_) => {
      console.error('There was a problem importing this Horse ' + horse.name)
    })
}

async function getOrCreateHorse (horseId) {
  let horse = await getLocalHorse(horseId)
  if (!horse.chrFEI && !horse.chrUSDF) {
    comment = comment + '\n Horse FEIID and USDFID are null for: ' + horse.chrName + ' - Competitor not imported!'
    throw new Error(`Horse FEIID and USDFID are null for: ${horse.chrName}`)
  }
  let horseBH = await api.horsesApi.findHorsesByLocalHorse(horse)
  if (!horseBH) {
    horseBH = await importMissingHorse(horse)
  }
  return horseBH
}

async function getOrCreateAthlete (riderId) {
  let rider = await getLocalRider(riderId)
  if (!rider.chrFEI && !rider.chrUSDF) {
    comment = comment + '\n Rider FEIID and USDFID are null for: ' + rider.chrFirstName + ' ' + rider.chrLastName + ' - Competitor not imported!'
    throw new Error(`Rider FEIID and USDFID are null for: ${rider.chrFirstName} ${rider.chrLastName}`)
  }
  let athleteBH = await api.athletesApi.findAthletesByLocalRider(rider)
  if (!athleteBH) {
    athleteBH = await importMissingAthlete(rider)
  }
  return athleteBH
}

async function checkCompetitors (competitions, localCompetitors) {
  for (var i = 0; i < competitions.length; i++) {
    const competitors = await api.competitorsApi.getDressageIndividualCompetitorByCompetition(competitions[i].id)
    const filteredFvCompetitors = _.filter(localCompetitors, function (c) { return c.chrText === competitions[i].number })
    await saveOrUpdateCompetitorsForCompetition(filteredFvCompetitors, competitions[i].id, competitors)
    await checkIfLocalCompetitorGotDeleted(filteredFvCompetitors, competitions[i].id, competitors)
  }
}

async function checkCompetitorsForCompetition (competition, localCompetitors) {
  const competitors = await api.competitorsApi.getDressageIndividualCompetitorByCompetition(competition.id)
  const filteredFvCompetitors = _.filter(localCompetitors, function (c) { return c.chrText === competition.number })
  await saveOrUpdateCompetitorsForCompetition(filteredFvCompetitors, competition.id, competitors)
  await checkIfLocalCompetitorGotDeleted(filteredFvCompetitors, competition.id, competitors)
}

async function checkIfLocalCompetitorGotDeleted (localCompetitors, competitionId, competitors) {
  // Filter all breaks
  let rideData = localCompetitors.filter((ride) => ride.idsRiderID !== -1 && ride.idsHorseID !== -1)

  for (var i = 0; i < competitors.length; i++) {
    let localEntryExisting = rideData.filter((ride) => ride.lngEntryID === competitors[i].cno)
    if (!localEntryExisting || localEntryExisting.length === 0) {
      comment = comment + '\n Competitor ID: ' + competitors[i].id + ' in competition ' + competitionId + ' has been deleted. Manual update required!'
    }
  }
}

async function saveOrUpdateCompetitorsForCompetition (localCompetitors, competitionId, competitors) {
  let checkedHorses = []
  let checkedAthletes = []
  try {
    // Filter all breaks
    let rideData = localCompetitors.filter((ride) => ride.idsRiderID !== -1 && ride.idsHorseID !== -1)

    for (var i = 0; i < rideData.length; i++) {
      try {
        let horseBH
        let athleteBH
        const cno = rideData[i].lngEntryID

        let competitorBH = competitors.filter((c) => c.cno === cno)

        if (!competitorBH || competitorBH.length === 0) {
          // Insert new competitor entry in BH database

          const filteredCheckAtheletes = checkedAthletes.filter((a) => a.localId === rideData[i].idsRiderID)
          const filteredCheckedHorses = checkedHorses.filter((h) => h.localId === rideData[i].idsHorseID)

          if (filteredCheckAtheletes && filteredCheckAtheletes.length > 0) {
            athleteBH = filteredCheckAtheletes[0].bhAthlete
          }

          if (filteredCheckedHorses && filteredCheckedHorses.length > 0) {
            horseBH = filteredCheckedHorses[0].bhHorse
          }

          if (!athleteBH) {
            athleteBH = await getOrCreateAthlete(rideData[i].idsRiderID)
            checkedAthletes.push({ localId: rideData[i].idsRiderID, bhAthlete: athleteBH })
          }
          if (!horseBH) {
            horseBH = await getOrCreateHorse(rideData[i].idsHorseID)
            checkedHorses.push({ localId: rideData[i].idsHorseID, bhHorse: horseBH })
          }

          if (athleteBH && horseBH) {
          // Convert competitionDate to UTC from the defined TimeZone. This assumes that the date already is
          // in the defined timeZone
          //  console.log('new athlete')
            let competitionDate = moment(rideData[0].dtmDate)
          //  console.log('rideTime for ' + athleteBH.firstName + ': ' + rideTime)
          //  console.log('originally in DB: ' + rideData[i].dtmTime)
            // this is BS:
            /*
saved competitor: 53498
new athlete
rideTime for Kerri: -2209110000000
originally in DB: 1899-12-30T14:20:00Z
competitionDate: 1610600400000
competitionTimeZone: America/New_York
momentTime: 2021-01-14 09:23:44
momentTimeTz: 1610634224000
we parsed this as: 2021-01-14 09:23:44 -05:00

            */
           // split dtmTime because all is lost with FVD
            let split = rideData[i].dtmTime.toString().substring(11, 20)
          //  console.log('split: ' + split)
          //  console.log('competitionDate: ' + competitionDate)
          //  console.log('competitionTimeZone: ' + competitionTimeZone)
            let momentTime = competitionDate.format('YYYY-MM-DD') + ' ' + split
          //  console.log('momentTime: ' + momentTime)
            let momentTimeTz = moment(momentTime).tz(competitionTimeZone, true)
          //  console.log('momentTimeTz: ' + momentTimeTz)
          //  console.log('we parsed this as: ' + momentTimeTz.format('YYYY-MM-DD HH:mm:ss Z'))
           // console.log('----')

            // both are existing
            let dressageIndividualCompetitor = {
              zonedInstant: {
                instant: momentTimeTz.tz('UTC').format(),
                timeZone: competitionTimeZone
              },
              cno: rideData[i].lngEntryID,
              athlete: {
                id: athleteBH.id
              },
              horse: {
                id: horseBH.id,
                name: horseBH.name
              },
              competition: {
                id: competitionId
              }
            }
            const savedCompetitor = await api.competitorsApi.saveDressageIndividualCompetitor(dressageIndividualCompetitor)
            competitors.push(savedCompetitor)
          }
        } else if (competitorBH && competitorBH.length > 0) {
        // check if competitor was updated
          const isUpcoming = competitorBH[0].status === 'UPCOMING'
          let isChanged = false
          let competitionDate = moment(rideData[0].dtmDate)
          let split = rideData[i].dtmTime.toString().substring(11, 20)
          let momentTime = competitionDate.format('YYYY-MM-DD') + ' ' + split
          let momentTimeTz = moment(momentTime).tz(competitionTimeZone, true)

          const filteredCheckAtheletes = checkedAthletes.filter((a) => a.localId === rideData[i].idsRiderID)
          const filteredCheckedHorses = checkedHorses.filter((h) => h.localId === rideData[i].idsHorseID)

          if (filteredCheckAtheletes && filteredCheckAtheletes.length > 0) {
            athleteBH = filteredCheckAtheletes[0].bhAthlete
          }

          if (filteredCheckedHorses && filteredCheckedHorses.length > 0) {
            horseBH = filteredCheckedHorses[0].bhHorse
          }

          if (!athleteBH) {
            athleteBH = await getOrCreateAthlete(rideData[i].idsRiderID)
            checkedAthletes.push({ localId: rideData[i].idsRiderID, bhAthlete: athleteBH })
          }
          if (!horseBH) {
            horseBH = await getOrCreateHorse(rideData[i].idsHorseID)
            checkedHorses.push({ localId: rideData[i].idsHorseID, bhHorse: horseBH })
          }

          if (competitorBH[0].zonedInstant.instant !== momentTimeTz.tz('UTC').format()) {
            isChanged = true
            competitorBH[0].zonedInstant.instant = momentTimeTz.tz('UTC').format()
          }
          if (competitorBH[0].horse.id !== horseBH.id) {
            comment = comment + '\n Competitor ID: ' + competitorBH[0].id + ' has a changed horse. Manual update required! (Competiton Id: ' + competitionId + ' )'
          }
          if (competitorBH[0].athlete.id !== athleteBH.id) {
            comment = comment + '\n Competitor ID: ' + competitorBH[0].id + ' has a changed athlete. Manual update required! (Competiton Id: ' + competitionId + ' )'
          }

          if (isChanged && isUpcoming) {
            comment = comment + '\n Competitor ' + competitorBH[0].athlete.firstName + '(ID: ' + competitorBH[0].id + ') had a changed time and was updated automatically'
            _.forEach(competitorBH[0].judgeResults, judgeResult => {
              // the server does not expect and accept markedExercises
              delete judgeResult.markedExercises
            })
            competitorBH[0] = await api.competitorsApi.saveDressageIndividualCompetitor(competitorBH[0])
          } else if (isChanged && !isUpcoming) {
            comment = comment + '\n Competitor ' + competitorBH[0].athlete.firstName + '(ID: ' + competitorBH[0].id + ') has a changed time but the status is ' +
            competitorBH[0].status + '. Manual update required! (Competiton Id: ' + competitionId + ' )'
          }
        }
      } catch (e) {
        console.error(e)
      }
    }
    return Promise.resolve('Competitors saved')
  } catch (e) {
    console.error(e)
  }
}

async function exportNewMarksNational () {
  for (var i = 0; i < nationalCompetitions.length; i++) {
    await manageCompetitionData(nationalCompetitions[i], classNationalDatabaseConnection)
  }
}

async function exportNewMarksInternational () {
  for (var i = 0; i < internationalCompetitions.length; i++) {
    if (internationalCompetitions[i].name?.toLowerCase().indexOf('para') === -1) {
      await manageCompetitionData(internationalCompetitions[i], classInternationalDatabaseConnection)
    } else {
      await manageCompetitionData(internationalCompetitions[i], classInternationalParaDatabaseConnection)
    }
  }
}

async function manageCompetitionData (competition, classDatabaseConnection) {
  let dispatcher = new SyncMarksDispatcher(competition.id, competition.number, classDatabaseConnection, token, serverAddress)

  const competitors = await api.competitorsApi.getDressageIndividualCompetitorByCompetition(competition.id)
  for (var i = 0; i < competitors.length; i++) {
    let competitor = competitors[i]
    emitStatus()
    await dispatcher.dispatch(competitor, competition)
      .then((_) => {
        log('Finished exporting ' + competitor.cno)
      })
      .catch((error) => {
        log('There was a problem exporting the data. Please check the logs')
        console.error(error)
      })
  }
  log('Finished exporting ' + competition.name)
}

io.on('connection', client => {
  client.on('credentials', data => {
    username = data.username
    password = data.password
    obtainToken()
  })
  client.on('importCompetition', async data => {
    try {
      competitionId = data.competitionId
      console.log(`got new competitionId for import: ${competitionId}`)
      comment = ''

      status = states.retrievingBlackHorseData
      emitStatus()
      let competition = await api.competitionsApi.getDressageCompetition(competitionId)

      status = states.retrievingLocalData
      emitStatus()
      let localCompetitionSource = await getLocalCompetitionSource(competition)
      if (localCompetitionSource.source === 'national') {
        fvNationalCompetitors = await getLocalCompetitors(classNationalDatabaseConnection)
      } else if (localCompetitionSource.source === 'international') {
        fvInternationalCompetitors = await getLocalCompetitors(classInternationalDatabaseConnection)
      } else {
        fvInternationalParaCompetitors = await getLocalCompetitors(classInternationalParaDatabaseConnection)
      }

      status = states.compareImportNationalCompetitors
      emitStatus()
      let localCompetitors = localCompetitionSource.source === 'national' ? fvNationalCompetitors : localCompetitionSource.source === 'international' ? fvInternationalCompetitors : fvInternationalParaCompetitors
      await checkCompetitorsForCompetition(competition, localCompetitors)

      status = states.done
      emitStatus()
    } catch (e) {
      console.error(e)
    }
  })
  client.on('exportCompetition', async data => {
    try {
      competitionId = data.competitionId
      console.log(`got new competitionId for export: ${competitionId}`)
      comment = ''

      status = states.retrievingBlackHorseData
      emitStatus()
      let competition = await api.competitionsApi.getDressageCompetition(competitionId)

      status = states.retrievingLocalData
      emitStatus()
      let localCompetitionSource = await getLocalCompetitionSource(competition)

      status = states.exportNewMarks
      emitStatus()
      let dbSource = localCompetitionSource.source === 'national' ? classNationalDatabaseConnection : localCompetitionSource.source === 'international' ? classInternationalDatabaseConnection : classInternationalParaDatabaseConnection
      await manageCompetitionData(competition, dbSource)

      status = states.done
      emitStatus()
    } catch (e) {
      console.error(e)
    }
  })
  client.on('nationalShowId', async data => {
    try {
      nationalShowId = data.nationalShowId
      console.log(`got new national showId: ${nationalShowId}`)
      comment = ''

      status = states.retrievingBlackHorseData
      emitStatus()
      let nationalResponse = await getBHCompetitions(nationalShowId)
      nationalShow = nationalResponse.show
      nationalCompetitions = nationalResponse.competitions

      status = states.retrievingLocalData
      emitStatus()
      fvNationalCompetitions = await getLocalCompetitions(classNationalDatabaseConnection)
      fvNationalCompetitors = await getLocalCompetitors(classNationalDatabaseConnection)

      status = states.compareImportCompetitions
      emitStatus()
      await saveOrUpdateCompetitons(fvNationalCompetitions, nationalCompetitions, nationalShow, 'national')
      // reload data from bh
      nationalResponse = await getBHCompetitions(nationalShowId)
      nationalShow = nationalResponse.show
      nationalCompetitions = nationalResponse.competitions

      status = states.compareImportNationalCompetitors
      emitStatus()
      await checkCompetitors(nationalCompetitions, fvNationalCompetitors)

      status = states.exportNewMarks
      emitStatus()
      await exportNewMarksNational()

      status = states.done
      emitStatus()
    } catch (e) {
      console.error(e)
    }
  })
  client.on('internationalShowId', async data => {
    try {
      internationalShowId = data.internationalShowId
      console.log(`got new international showId: ${internationalShowId}`)
      comment = ''

      status = states.retrievingBlackHorseData
      emitStatus()
      let internationalResponse = await getBHCompetitions(internationalShowId)
      internationalShow = internationalResponse.show
      internationalCompetitions = internationalResponse.competitions

      status = states.retrievingLocalData
      emitStatus()
      fvInternationalCompetitions = await getLocalCompetitions(classInternationalDatabaseConnection)
      fvInternationalParaCompetitions = await getLocalCompetitions(classInternationalParaDatabaseConnection)
      fvInternationalCompetitors = await getLocalCompetitors(classInternationalDatabaseConnection)
      fvInternationalParaCompetitors = await getLocalCompetitors(classInternationalParaDatabaseConnection)

      status = states.compareImportCompetitions
      emitStatus()
      await saveOrUpdateCompetitons(fvInternationalCompetitions, internationalCompetitions, internationalShow, 'international')
      if (fvInternationalParaCompetitions && fvInternationalParaCompetitions.length > 0) {
        await saveOrUpdateCompetitons(fvInternationalParaCompetitions, internationalCompetitions, internationalShow, 'international')
      }

      // reload data from bh
      internationalResponse = await getBHCompetitions(internationalShowId)
      internationalShow = internationalResponse.show
      internationalCompetitions = internationalResponse.competitions

      status = states.compareImportInternationalCompetitors
      emitStatus()
      const filteredCompetitions = _.filter(internationalCompetitions, function (c) { return c.name?.toLowerCase().indexOf('para') === -1 })
      await checkCompetitors(filteredCompetitions, fvInternationalCompetitors)
      const filteredParaCompetitions = _.filter(internationalCompetitions, function (c) { return c.name?.toLowerCase().indexOf('para') !== -1 })
      await checkCompetitors(filteredParaCompetitions, fvInternationalParaCompetitors)

      status = states.exportNewMarks
      emitStatus()
      await exportNewMarksInternational()

      status = states.done
      emitStatus()
    } catch (e) {
      console.error(e)
    }
  })
  client.on('disconnect', () => { console.log('client disconnected') })
})

io.on('connect', (socket) => {
  console.log('client connected')
  emitStatus()
})

io.listen(3088)
console.log('ready for connections')
