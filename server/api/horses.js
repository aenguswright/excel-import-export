const moment = require('moment-timezone')

class Horses {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.horseParams = `{
      id
      name
      sire
      dam
      sireOfSire
      damOfSire
      sireOfDam
      damOfDam
      dateOfBirth
      studbook
      permanentBridleNumber
      nationalRegistrations {
        number
        federation {
          shorthand
          nation {
            ioc
          }
        }
      }
      feiId
      owner
      breeder
      microChipNumber
    }`

    this.saveHorseMutation = `mutation CreateOrSave ($horse: HorseInput!) {
      horse: saveHorse(horse: $horse) ` + this.horseParams + `
    }`
  }

  getHorse (id) {
    let graphQl = this.graphQl
    let query = `{
      horse(id: ` + id + `)` + this.horseParams + `
    }`
    return new Promise(function (resolve, reject) {
      graphQl.request(query).then(response => {
        let horse = response.horse
        if (horse) {
          resolve(horse)
        } else {
          reject(new Error('horse was undefined'))
        }
      })
    })
  }

  getNationalRegistrationNumber(horse) {
    if (!horse.nationalRegistrations || horse.nationalRegistrations.length == 0) {
      return false
    } else {
      let nfNrs = horse.nationalRegistrations.filter(nr => nr.federation && nr.federation.ioc)
      // try to find USDF first
      let usdf = nfNrs.filter(nr => nr.federation.shorthand?.toLowerCase() === 'usdf')
      if (usdf.length > 0) {
        return usdf[0].number
      }
      // try to find USEF
      let usef = nfNrs.filter(nr => nr.federation.shorthand?.toLowerCase() === 'usef')
      if (usef.length > 0) {
        return usef[0].number
      }
      // try any US
      let us = nfNrs.filter(nr => nr.federation.ioc?.toLowerCase() === 'usa')
      if (us.length > 0) {
        return us[0].number
      }
      // take any
      return horse.nationalRegistrations[0].number
    }
  }

  async saveNationalRegistration (number) {
    // MANUALLY MAPPED - ADAPT WHEN WE USE A DIFFERENT SERVER - currently we only get the USDF NRN
    let graphQl = this.graphQl
    let variables = {
      registration: {
        number: number,
        type: 'HORSE',
        federation: { id: 35 }
      }
    }
    let query = `mutation SaveNationalRegistration($registration: NationalRegistrationInput!) {
      nrn: saveNationalRegistration(registration: $registration) { id }
    }`
    return graphQl.request(query, variables).then(response => {
      let nrn = response.nrn
      return nrn.id
    })
  }

  async findHorsesByLocalHorse (horse) {
    if (horse.chrFEI) {
      let remoteHorse = await this.findHorsesByFeiId(horse.chrFEI)
      if (remoteHorse) {
        if (!this.getNationalRegistrationNumber(remoteHorse) && horse.chrUSDF) {
          remoteHorse.nationalRegistrations = []
          console.log('about to save missing nationalRegistrationNumber for ' + remoteHorse.name + '. Adding NRN = ' + horse.chrUSDF)
          remoteHorse.nationalRegistrations.push({
            id: await this.saveNationalRegistration(horse.chrUSDF)
          })
          await this.saveHorse(remoteHorse)
          console.log('saved missing nationalRegistrationNumber for horse ' + remoteHorse.name + '. Adding NRN = ' + horse.chrUSDF)
        }
        return Promise.resolve(remoteHorse)
      } else if (horse.chrUSDF && horse.chrFEI) {
        let horseBH = await this.findHorsesByNationalRegistrationNumber(horse.chrUSDF)
        if (horseBH && !horseBH.feiId) {
          horseBH.feiId = horse.chrFEI
          await this.saveHorse(horseBH)
          console.log('saved missing Fei ID for horse ' + horseBH.name + '. Adding FEI ID = ' + horse.chrFEI)
        }
        return horseBH
      }
    } else if (horse.chrUSDF) {
      let horses = await this.findHorsesByNationalRegistrationNumber(horse.chrUSDF)
      if (horses) {
        return Promise.resolve(horses)
      }
    }
  }

  async findHorsesByNationalRegistrationNumber (nationalRegistrationNumber) {
    let graphQl = this.graphQl
    let variables = { nationalRegistrationNumber: nationalRegistrationNumber, ioc: 'usa' }
    let query = `query GetHorses($nationalRegistrationNumber: String!, $ioc: String!) {
      horses: findHorsesByNationalRegistrationNumberAndFederationIoc(nationalRegistrationNumber: $nationalRegistrationNumber, ioc: $ioc) ` + this.horseParams + `
    }`
    return graphQl.request(query, variables).then(response => {
      let horses = response.horses
      if (horses && horses.length > 0) {
        // take the first one
        return horses[0]
      } else {
        return null
      }
    })
  }

  async findHorsesByFeiId (feiId) {
    let graphQl = this.graphQl
    let variables = { feiId: feiId }
    let query = `query GetHorses($feiId: String!) {
      findHorse: horseByFeiId(feiId: $feiId) ` + this.horseParams + `
    }`
    return graphQl.request(query, variables).then(response => {
      let horse = response.findHorse
      return horse
    })
  }

  async saveHorse (horse) {
    if (horse.dateOfBirth && horse.dateOfBirth !== '') {
      horse.dateOfBirth = moment.tz(horse.dateOfBirth, 'America/New_York').utc().format()
    }
    const variables = {
      horse: horse
    }
    return this.graphQl.request(this.saveHorseMutation, variables)
  }

  fromMDB (mdbHorse) {
    return {
      feiId: mdbHorse.chrFEI,
      name: mdbHorse.chrName,
      sire: mdbHorse.chrSire,
      dam: mdbHorse.chrDam,
      sireOfDam: mdbHorse.chrDamsSire,
      studbook: mdbHorse.chrBreedID,
      nationalRegistrationNumber: mdbHorse.chrUSDF,
      dateOfBirth: mdbHorse.intBirthYear ? moment.tz(`${mdbHorse.intBirthYear}-01-01`, 'America/New_York').utc().format() : null
    }
  }
}

module.exports = {
  Horses: Horses
}
