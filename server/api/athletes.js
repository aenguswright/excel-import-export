const moment = require('moment-timezone')
const { Nations } = require('./nations.js')

class Athletes {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.athleteParams = `{
      id
      firstName
      familyName
      dateOfBirth
      feiId
      nationalRegistrations {
        number
        federation {
          shorthand
          nation {
            ioc
          }
        }
      }
      bio
      imageUrl
      allowsMails
      comments {
        givenAt {
          id
          name
        }
        submissionDate
        message
      }
      contacts {
        firstName
        familyName
        email
        phone
        type
        allowsMails
      }
      nation {
        id
        ioc
        name
      }
      email
      phone
    }`

    this.saveAthleteMutation = `mutation CreateOrSave ($athlete: AthleteInput!) {
      athlete: saveAthlete(athlete: $athlete) ` + this.athleteParams + `
    }`
  }

  getAthlete (id) {
    let graphQl = this.graphQl
    let query = `{
      athlete(id: ` + id + `)` + this.athleteParams + `
    }`
    return new Promise(function (resolve, reject) {
      graphQl.request(query).then(response => {
        let athlete = response.athlete
        if (athlete) {
          resolve(athlete)
        } else {
          reject(new Error('athlete was undefined'))
        }
      })
    })
  }

  getNationalRegistrationNumber(athlete) {
    if (!athlete.nationalRegistrations || athlete.nationalRegistrations.length == 0) {
      return false
    } else {
      let nfNrs = athlete.nationalRegistrations.filter(nr => nr.federation && nr.federation.ioc)
      // try to find USDF first
      let usdf = nfNrs.filter(nr => nr.federation.shorthand.toLowerCase() === 'usdf')
      if (usdf.length > 0) {
        return usdf[0].number
      }
      // try to find USEF
      let usef = nfNrs.filter(nr => nr.federation.shorthand.toLowerCase() === 'usef')
      if (usef.length > 0) {
        return usef[0].number
      }
      // try any US
      let us = nfNrs.filter(nr => nr.federation.ioc.toLowerCase() === 'usa')
      if (us.length > 0) {
        return us[0].number
      }
      // take any
      return athlete.nationalRegistrations[0].number
    }
  }

  async saveNationalRegistration (number) {
    // MANUALLY MAPPED - ADAPT WHEN WE USE A DIFFERENT SERVER - currently we only get the USDF NRN
    let graphQl = this.graphQl
    let variables = {
      registration: {
        number: number,
        type: 'ATHLETE',
        federation: { id: 35 }
      }
    }
    let query = `mutation SaveNationalRegistration($registration: NationalRegistrationInput!) {
      nrn: saveNationalRegistration(registration: $registration) { id }
    }`
    return graphQl.request(query, variables).then(response => {
      let nrn = response.nrn
      return nrn.id
    })
  }

  async findAthletesByLocalRider (rider) {
    if (rider.chrFEI) {
      let athlete = await this.findAthletesByFeiId(rider.chrFEI)
      if (athlete) {
        if (!this.getNationalRegistrationNumber(athlete) && rider.chrUSDF) {
          athlete.nationalRegistrations = []
          console.log('about to save missing nationalRegistrationNumber for ' + athlete.firstName + ' ' + athlete.familyName + '. Adding NRN = ' + rider.chrUSDF)
          athlete.nationalRegistrations.push({
            id: await this.saveNationalRegistration(rider.chrUSDF)
          })
          await this.saveAthlete(athlete)
          console.log('saved missing nationalRegistrationNumber for ' + athlete.firstName + ' ' + athlete.familyName + '. Adding NRN = ' + rider.chrUSDF)
        }
        return Promise.resolve(athlete)
      } else if (rider.chrUSDF && rider.chrFEI) {
        let athlete = await this.findAthletesByNationalRegistrationNumber(rider.chrUSDF)
        if (athlete && !athlete.feiId) {
          athlete.feiId = rider.chrFEI
          await this.saveAthlete(athlete)
          console.log('saved missing Fei ID for ' + athlete.firstName + ' ' + athlete.familyName + '. Adding FEI ID = ' + rider.chrFEI)
        }
        return athlete
      }
    } else if (rider.chrUSDF) {
      let athlete = await this.findAthletesByNationalRegistrationNumber(rider.chrUSDF)
      if (athlete) {
        return Promise.resolve(athlete)
      }
    }
  }

  async saveAthlete (athlete) {
    if (athlete.dateOfBirth && athlete.dateOfBirth !== '') {
      athlete.dateOfBirth = moment.tz(athlete.dateOfBirth, 'America/New_York').utc().format()
    }
    const variables = {
      athlete: athlete
    }
    return this.graphQl.request(this.saveAthleteMutation, variables)
  }

  async findAthletesByNationalRegistrationNumber (nationalRegistrationNumber) {
    let graphQl = this.graphQl
    let variables = { nationalRegistrationNumber: nationalRegistrationNumber, ioc: 'usa' }
    // we are generally happy if we find a rider with a fitting NRN of any US NF
    let query = `query GetAthletes($nationalRegistrationNumber: String!, $ioc: String!) {
      athletes: findAthletesByNationalRegistrationNumberAndFederationIoc(nationalRegistrationNumber: $nationalRegistrationNumber, ioc: $ioc) ` + this.athleteParams + `
    }`
    return graphQl.request(query, variables).then(response => {
      let athletes = response.athletes
      if (athletes && athletes.length > 0) {
        // just take the first one
        return athletes[0]
      } else {
        return null
      }
    })
  }

  async findAthletesByFeiId (feiId) {
    let graphQl = this.graphQl
    let variables = { feiId: feiId }
    let query = `query GetAthletes($feiId: Int!) {
      findAthlete: athleteByFeiId(feiId: $feiId) ` + this.athleteParams + `
    }`
    return graphQl.request(query, variables).then(response => {
      let athlete = response.findAthlete
      return athlete
    })
  }

  async fromMDB (mdbAthlete) {
    let nationsApi = new Nations(this.graphQl)
    return nationsApi.findNations(mdbAthlete.chrCitizenship || mdbAthlete.chrCountry)
      .then((nations) => {
        let returnObj = {
          firstName: mdbAthlete.chrFirstName,
          familyName: mdbAthlete.chrLastName,
          feiId: mdbAthlete.chrFEI,
          email: mdbAthlete.chrEmail,
          phone: mdbAthlete.chrTelephone,
          nationalRegistrationNumber: mdbAthlete.chrUSDF,
          allowsMails: true
        }
        if (mdbAthlete.dtmBirth !== '1970-01-01T00:00:00Z') {
          returnObj.dateOfBirth = mdbAthlete.dtmBirth
        }
        // if there is more than one nation, filter on ioc
        if (nations && nations.length > 1) {
          nations = nations.filter(nation => nation.ioc === mdbAthlete.chrCitizenship || nation.ioc === mdbAthlete.chrCountry)
        }
        if (nations && nations.length > 0) {
          returnObj.nation = {
            id: nations[0].id
          }
        } else {
          console.error(`No nation found for rider: ${mdbAthlete.chrFirstName} ${mdbAthlete.chrLastName}`)
        }
        return Promise.resolve(returnObj)
      }).catch((error) => {
        console.error(error)
      })
  }
}

module.exports = {
  Athletes: Athletes
}
