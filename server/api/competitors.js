class Competitors {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.dressageCompetitorFragment = `fragment dressageCompetitor on DressageCompetitor {
        id
        cno
        status
        zonedInstant {
          instant
          timeZone
        }
        numberInCompetition
        rankOfficial
        rankAudience
        resultOfficialStatus
        resultAudience
        resultOfficial
        resultOfficialTechnical
        resultOfficialArtistic
        resultOfficialPoints
        resultOfficialTechnicalPoints
        resultOfficialArtisticPoints
        division
        judgeResults {
          id
          finalScore
          status
          judgeRank
          firstErrorOfCourse
          secondErrorOfCourse
          technicalPenalty
          markedExercises {
            id
            mark
            number
            type
            exercise{
              id
              number
              coefficient
            }
          }
          competitionJudge {
            position
            judge {
              id
            }
          }
        }
      }`

    this.dressageIndividualCompetitorFragment = `fragment dressageIndividualCompetitor on DressageIndividualCompetitor {
      individualTest {
          id
          name
          penaltyRule
          testType
          textAfter
          individualizedName
          exercises {
            id
            number
            exerciseType
            coefficient
          }
        }
        competition {
          id
          name
        }
        testOfChoiceAssignments {
          id
          test {
            id
            name
            penaltyRule
            testType
            exercises{
              id
              number
              exerciseType
              coefficient
            }
          }
          position
        }
        athlete {
          id
          firstName
          familyName
          nation {
            ioc
            name
          }
        }
        horse {
          id
          name
        }  
      }`

    this.jumpingCompetitorFragment = `fragment jumpingCompetitor on JumpingCompetitor {
        id
        cno
        status
        zonedInstant {
          instant
          timeZone
        }
        numberInCompetition
      }`

    this.jumpingIndividualCompetitorFragment = `fragment jumpingIndividualCompetitor on JumpingIndividualCompetitor {
        athlete {
          id
          firstName
          familyName
          nation {
            ioc
            name
          }
        }
        horse {
          id
          name
        }  
      }`

    this.saveDressageIndividualCompetitorMutation = `mutation CreateOrSave($competitor: DressageIndividualCompetitorInput!) {
        competitor: saveDressageIndividualCompetitor(competitor: $competitor) {
          ...dressageCompetitor
          ...dressageIndividualCompetitor
        }  
      }`

    this.getDressageIndividualCompetitorQuery = `query getIndividualCompetitor($id: ID!) {
        competitor: dressageIndividualCompetitor(id: $id) {
          ...dressageCompetitor
          ...dressageIndividualCompetitor
        }  
      }`

    this.getDressageIndividualCompetitorByCompetitionQuery = `query getIndividualCompetitor($competitionId: ID!) {
      competitor: dressageIndividualCompetitors(competitionId: $competitionId) {
        ...dressageCompetitor
        ...dressageIndividualCompetitor
      }  
    }`

    this.saveJumpingIndividualCompetitorMutation = `mutation CreateOrSave($competitor: JumpingIndividualCompetitorInput!) {
        competitor: saveJumpingIndividualCompetitor(competitor: $competitor) {
          ...jumpingCompetitor
          ...jumpingIndividualCompetitor
        }  
      }`

    this.getJumpingCompetitionQuery = `query getIndividualCompetitor($id: ID!) {
        competitor: jumpingIndividualCompetitor(id: $id) {
          ...jumpingCompetitor
          ...jumpingIndividualCompetitor
        }  
      }`

    this.deleteCourseMutation = `mutation DeleteCompetitor ($id: ID!) {
        status: deleteDressageCompetitor(id: $id)
      }`
  }

  getDressageIndividualCompetitor (id) {
    const variables = {
      id: id
    }
    let graphQl = this.graphQl
    let query = this.getDressageIndividualCompetitorQuery + this.dressageIndividualCompetitorFragment + this.dressageCompetitorFragment
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        let competitor = response.competitor
        if (competitor) {
          resolve(competitor)
        } else {
          reject(new Error('competitor was undefined'))
        }
      })
    })
  }

  getJumpingIndividualCompetitor (id) {
    const variables = {
      id: id
    }
    let graphQl = this.graphQl
    let query = this.getJumpingIndividualCompetitorQuery + this.jumpingIndividualCompetitorFragment + this.jumpingCompetitorFragment
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        let competitor = response.competitor
        if (competitor) {
          resolve(competitor)
        } else {
          reject(new Error('competitor was undefined'))
        }
      })
    })
  }

  async getDressageIndividualCompetitorByCompetition (competitionId) {
    const variables = {
      competitionId: competitionId
    }
    let graphQl = this.graphQl
    let query = this.getDressageIndividualCompetitorByCompetitionQuery + this.dressageIndividualCompetitorFragment + this.dressageCompetitorFragment
    return graphQl.request(query, variables).then(response => {
      let competitor = response.competitor
      return competitor
    })
  }

  async saveDressageIndividualCompetitor (competitor) {
    const variables = {
      competitor: competitor
    }
    return this.graphQl.request(this.saveDressageIndividualCompetitorMutation + this.dressageIndividualCompetitorFragment + this.dressageCompetitorFragment, variables)
      .then(response => {
        console.log('saved competitor: ' + response.competitor.id)
        return response.competitor
      }).catch((error) => {
        console.error(error)
      })
  }

  saveJumpingIndividualCompetitor (competitor) {
    const variables = {
      competitor: competitor
    }
    this.graphQl.request(this.saveJumpingIndividualCompetitorMutation + this.jumpingIndividualCompetitorFragment + this.jumpingCompetitorFragment, variables)
      .then(response => {
        return response.competitor
      })
  }
}

module.exports = {
  Competitors: Competitors
}
