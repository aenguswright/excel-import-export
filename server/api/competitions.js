const moment = require('moment-timezone')
const _ = require('lodash')

class Competitions {
  constructor (graphQl) {
    this.graphQl = graphQl
    this.competitionFragment = `fragment competitionParams on Competition {
      id
      name
      zonedInstant {
        instant
        timeZone
      }
      number
      status
      discipline
      sponsor {
        id
        name
      }
      league {
        id
        name
      }
      feiCompetitionCode
    }`
    this.dressageCompetitionParams = `
    fullModeEnabled
    shortModeEnabled
    oneModeEnabled
    youngHorsesModeEnabled
    supportsCompetitorDetails
    individualModeEnabled
    transferEveryMark
    displayStartlistInApp
    resultVisualization
    paperless
    competitionJudges {
      id
      position
      testMode
      judge {
        id
        firstName
        familyName
      }
    }
    fullModeTest {
      id
      name
      testType
      penaltyRule
      exercises{
        id
        number
        exerciseType
        coefficient
      }
    }
    shortModeTest {
      id
      name
      testType
      exercises{
        id
        number
        exerciseType
        coefficient
      }
    }
    oneModeTest {
      id
      name
      testType
      exercises{
        id
        number
        exerciseType
        coefficient
      }
    }
    youngHorsesTest {
      id
      name
      testType
      exercises{
        id
        number
        exerciseType
        coefficient
      }
    }`

    this.jumpingCompetitionParams = `
      obstacleCountSelection
      course {
        id
        obstacles {
          id
          name
          number
        }
      }
      winnerSelection
      clearRoundsSelection
      favoriteRiderSelection
      selectionsClosed
      officialWinner {
        id
      }
      officialClearRoundsCount
      officialMostMistakesObstacle {
        id
      }
      officialMostMistakesObstacleCount
      styleJudgingEnabled
      competitors {
        id
        ... on JumpingIndividualCompetitor {
          athlete {
            firstName
            familyName
          }
          horse {
            name
          }
        }
      }`

    this.saveDressageIndividualCompetition = `mutation CreateOrSave($competition: DressageIndividualCompetitionInput!) {
      competition: saveDressageIndividualCompetition(competition: $competition) {
        ...competitionParams
        ` + this.dressageCompetitionParams + `
      }
    }`

    this.getDressageCompetitionQuery = `query GetCompetition($id: ID!) {
      competition: dressageCompetition(id: $id) {
          ...competitionParams
          ` + this.dressageCompetitionParams + `
        }
    }`

    this.deleteCourseMutation = `mutation DeleteCourse($competitionId: ID!) {
      deleteJumpingCourse (competitionId: $competitionId)
    }`

    this.saveJumpingIndividualCompetition = `mutation CreateOrSave($competition: JumpingIndividualCompetitionInput!) {
      competition: saveJumpingIndividualCompetition(competition: $competition) {
        ...competitionParams
        ` + this.jumpingCompetitionParams + `
      }
    }`

    this.getJumpingCompetitionQuery = `query GetCompetition ($id: ID!) {
      competition: jumpingCompetition(id: $id) {
        ...competitionParams
        ` + this.jumpingCompetitionParams + `
      }
    }`
  }

  getDressageCompetition (id) {
    const variables = {
      id: id
    }
    let graphQl = this.graphQl
    let query = this.getDressageCompetitionQuery + this.competitionFragment
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        let competition = response.competition
        if (competition) {
          resolve(competition)
        } else {
          reject(new Error('competition was undefined'))
        }
      })
    })
  }

  getJumpingCompetition (id) {
    const variables = {
      id: id
    }
    let graphQl = this.graphQl
    let query = this.getJumpingCompetitionQuery + this.competitionFragment
    return new Promise(function (resolve, reject) {
      graphQl.request(query, variables).then(response => {
        let competition = response.competition
        if (competition) {
          resolve(competition)
        } else {
          reject(new Error('competition was undefined'))
        }
      })
    })
  }

  prepareForSaving (competition) {
    competition.zonedInstant.instant = moment(competition.zonedInstant.instant)
      .tz(competition.zonedInstant.timeZone, true).utc().format()
    delete competition.lastModified
    delete competition.modifiedByUser
    if (competition.competitionJudges) {
      _.forEach(competition.competitionJudges, competitionJudge => {
        delete competitionJudge.judge.lastModified
      })
    }
  }

  saveDressageCompetition (competition) {
    this.prepareForSaving(competition)
    this.graphQl.request(this.saveDressageIndividualCompetition + this.competitionFragment, {
      competition: competition
    }).then(response => {
      return response.competition
    })
      .catch((error) => {
        console.error(`Failed to save competitions because of:`, error)
      })
  }

  saveJumpingCompetition (competition) {
    this.prepareForSaving(competition)
    const variables = {
      competition: competition
    }
    this.graphQl.request(this.saveJumpingIndividualCompetition + this.competitionFragment, variables).then(response => {
      return response.competition
    })
  }
}

module.exports = {
  Competitions: Competitions
}
