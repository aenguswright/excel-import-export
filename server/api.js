const { GraphQLClient } = require('graphql-request')
const { Competitions } = require('./api/competitions')
const { Competitors } = require('./api/competitors')
const { Athletes } = require('./api/athletes')
const { Horses } = require('./api/horses')
const { Nations } = require('./api/nations.js')
const { Shows } = require('./api/shows.js')

class Api {
  constructor (serverAddress, token) {
    this.token = token
    this.graphQl = new GraphQLClient(`${serverAddress}api/graph/`, {
      headers: {
        Authorization: token
      }
    })
    this.competitionsApi = new Competitions(this.graphQl)
    this.competitorsApi = new Competitors(this.graphQl)
    this.athletesApi = new Athletes(this.graphQl)
    this.horsesApi = new Horses(this.graphQl)
    this.nationsApi = new Nations(this.graphQl)
    this.showsApi = new Shows(this.graphQl)
  }
}

module.exports = {
  Api: Api
}
